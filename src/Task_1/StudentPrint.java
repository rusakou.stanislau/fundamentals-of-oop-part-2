package Task_1;

import java.util.Arrays;

public class StudentPrint {

    private Student[] students;
    private int targetGrade;

    public StudentPrint(Student[] students, int targetGrade) {
        this.students = students;
        this.targetGrade = targetGrade;
    }

    public void printStudentsWithOnlyGradesAsTarget() {
        String result;

        if (students.length > 0) {
            result = Arrays.toString(students);
        } else {
            result = "There are no students with grades only equal or higher than " + targetGrade;
        }

        System.out.println(result);
    }
}
