package Task_1;

import java.util.Arrays;

public class Student {
    private String surname;
    private String name;
    private int groupNumber;
    private int[] grades;

    public Student(String surname, String name, int groupNumber, int[] grades) {
        this.surname = surname;
        this.name = name;
        this.groupNumber = groupNumber;
        this.grades = grades;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(int groupNumber) {
        this.groupNumber = groupNumber;
    }

    public int[] getGrades() {
        return grades;
    }

    public void setGrades(int[] grades) {
        this.grades = grades;
    }

    @Override
    public String toString() {
        return "Student{" +
                "surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", groupNumber=" + groupNumber +
                ", grades=" + Arrays.toString(grades) +
                '}';
    }
}
