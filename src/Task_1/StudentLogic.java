package Task_1;

public class StudentLogic {
    private Student[] students;
    private int targetGrade;

    public StudentLogic(Student[] students, int targetGrade) {
        this.students = students;
        this.targetGrade = targetGrade;
    }

    public Student[] findWithAllGradesGreaterThanTarget() {
        Student[] result = new Student[10];

        int count = 0;
        for (Student student : students) {
            int[] grades = student.getGrades();

            if (isAllGreaterOrEqualThanTarget(grades, targetGrade)) {
                result[count++] = student;
            }
        }

        Student[] destination = new Student[count];
        System.arraycopy(result,0,destination,0,count);
        return destination;
    }

    private static boolean isAllGreaterOrEqualThanTarget(int[] grades, int target) {
        for (int grade : grades) {
            if (grade < target) {
                return false;
            }
        }

        return true;
    }
}
