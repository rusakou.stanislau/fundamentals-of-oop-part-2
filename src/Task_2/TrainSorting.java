package Task_2;

public class TrainSorting {

    public static Train[] sortTrainsByNumber(Train[] trains) {
        int indexOfLeast;
        Train temp;

        for (int i = 0; i < trains.length - 1; i++) {
            indexOfLeast = i;

            for (int j = i + 1; j < trains.length; j++) {
                int numberOfCurrentTrain = trains[indexOfLeast].getTrainNumber();
                int numberOfTrainToCompare = trains[j].getTrainNumber();

                if (numberOfTrainToCompare < numberOfCurrentTrain) {
                    indexOfLeast = j;
                }
            }

            temp = trains[indexOfLeast];
            trains[indexOfLeast] = trains[i];
            trains[i] = temp;
        }

        return trains;
    }

    public static Train[] sortByDestinationAndDeparture(Train[] trains) {
        boolean isSortedByDestanation = false;

        while (!isSortedByDestanation) {
            isSortedByDestanation = true;

            for (int i = 0; i < trains.length - 1; i++) {
                Train tempTrain = trains[i];
                Train trainToCompare = trains[i + 1];

                boolean isTempTrainDestAndDepLarger = compareTrainsByDestinationAndDeparture(tempTrain, trainToCompare);

                if (isTempTrainDestAndDepLarger) {
                    isSortedByDestanation = false;

                    trains[i] = trainToCompare;
                    trains[i + 1] = tempTrain;
                }
            }
        }

        return trains;
    }

    private static boolean compareTrainsByDestinationAndDeparture(Train tempTrain, Train trainToCompare) {
        String tempDestination = tempTrain.getDestination();
        String destinationToCompare = trainToCompare.getDestination();

        String tempDeparture = tempTrain.getDeparture();
        String departureToCompare = trainToCompare.getDeparture();

        boolean isCurrentDestinationBigger = tempDestination.compareTo( destinationToCompare ) > 0;
        boolean areDestinationsEqual = tempDestination.compareTo( destinationToCompare ) == 0;
        boolean isCurrentDepartureBigger = tempDeparture.compareTo( departureToCompare ) > 0;

        boolean result = isCurrentDestinationBigger || (areDestinationsEqual && isCurrentDepartureBigger);
        return result;
    }

}
