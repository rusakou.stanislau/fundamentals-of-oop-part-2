package Task_2;

public class Train {

    private String destination;
    private int trainNumber;
    private String departure;

    public Train(String destination, int trainNumber, String departure) {
        this.destination = destination;
        this.trainNumber = trainNumber;
        this.departure = departure;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(int trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    @Override
    public String toString() {
        return "Train{" +
                "destination='" + destination + '\'' +
                ", trainNumber=" + trainNumber +
                ", departure='" + departure + '\'' +
                '}';
    }
}
