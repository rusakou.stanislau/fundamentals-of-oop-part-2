package Task_2;

import java.util.Arrays;

public class TrainPrints {

    public static void printTrains(Train[] trains) {
        System.out.println(Arrays.toString(trains));
    }

    public static void printTrainByNumber(Train[] trains, int targetNumber) {
        for (Train train : trains) {
            int trainNumber = train.getTrainNumber();

            if (trainNumber == targetNumber) {
                printTrains(new Train[]{train});
                break;
            }
        }
    }
}
