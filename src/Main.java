import Task_1.Student;
import Task_1.StudentLogic;
import Task_1.StudentPrint;
import Task_2.Train;
import Task_2.TrainPrints;
import Task_2.TrainSorting;
import Task_3.Customer;
import Task_3.CustomerAggregation;
import Task_3.CustomerPrint;

public class Main {


    public static void main(String[] args) {
        runStudentTask();
        runTrainTask();
        runCustomerTask();
    }

    public static void runTrainTask() {
        String[] destinations = new String[]{"Rome","Milan","Rome","Sicily","Tourino"};
        String[] departures = new String[]{"19:30","10:00","09:00","07:00","22:45"};
        int[] numbers = new int[]{193,101,901,172,234};

        Train[] trains = new Train[5];

        for (int i = 0; i < 5; i++) {
            Train train = new Train(destinations[i], numbers[i], departures[i]);
            trains[i] = train;
        }

        Train[] sortedByNumbers = TrainSorting.sortTrainsByNumber(trains);
        TrainPrints.printTrains(sortedByNumbers);
        TrainPrints.printTrainByNumber(sortedByNumbers, 234);

        Train[] sortedByDestination = TrainSorting.sortByDestinationAndDeparture(trains);
        TrainPrints.printTrains(sortedByDestination);
    }

    public static void runStudentTask() {
        String[] firstNames = new String[]{"Petr","Jan","Barbara","Linus","James","Uladzimir","Johan","Steven","Emanuel","Andrew"};
        String[] surNames = new String[]{"Kovalov","Kowalski","Radzivil","Torvalds","Gosling","Zaremba","Shmidt","Johanson","Macron","Duda"};

        Student[] students = new Student[10];

        for (int i = 0; i < 10; i++) {
            Student student = new Student(surNames[i],firstNames[i],i,generateGrades());
            students[i] = student;
        }

        StudentLogic studentLogic = new StudentLogic(students, 9);
        Student[] studentsWithSatisfiedGrades = studentLogic.findWithAllGradesGreaterThanTarget();

        StudentPrint printer = new StudentPrint(studentsWithSatisfiedGrades, 9);
        printer.printStudentsWithOnlyGradesAsTarget();
    }

    private static int getRandomGradeNumber() {
        int grade = (int) ((Math.random() * (10 - 1)) + 1);
        return grade;
    }

    private static int[] generateGrades() {
        int[] grades = new int[5];

        for (int i = 0; i < 5; i++) {
            grades[i] = getRandomGradeNumber();
        }

        return grades;
    }

    public static void runCustomerTask() {
        String[] firstNames = new String[]{"Petr","Jan","Jan","Linus","James"};
        String[] surNames = new String[]{"Kovalov","Kowalski","Kowalski","Torvalds","Gosling"};
        String[] patronymics = new String[]{"Kov","Kowalski","Zowalski","Ivanovich","Javovich"};

        Customer[] customers = new Customer[5];
        for (int i = 0; i < 5; i++) {
            Customer customer = new Customer(i, surNames[i], firstNames[i], patronymics[i], i + " street", i + i, i * 10);
            customers[i] = customer;
        }

        CustomerAggregation customersBase = new CustomerAggregation(customers);
        customersBase.getCustomersSortedByNames();
        CustomerPrint.printCustomers(customersBase.getCustomers());

        customersBase.selectCustomersByCreditCardRange(1, 4);
        CustomerPrint.printCustomers(customersBase.getCustomers());
    }
}