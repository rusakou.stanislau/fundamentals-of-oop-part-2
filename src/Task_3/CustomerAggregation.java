package Task_3;

public class CustomerAggregation {

    private Customer[] customers;

    public CustomerAggregation(Customer[] customers) {
        this.customers = customers;
    }

    public void setCustomers(Customer[] customers) {
        this.customers = customers;
    }

    public Customer[] getCustomers() {
        return customers;
    }

    public void selectCustomersByCreditCardRange(int minCardNumber, int maxCardNumber) {
        Customer[] result = new Customer[customers.length];

        int count = 0;
        for (Customer customer : customers) {
            int creditCard = customer.getCreditCard();
            boolean isCreditCardFitToRange = creditCard >= minCardNumber && creditCard <= maxCardNumber;

            if (isCreditCardFitToRange) {
                result[count++] = customer;
            }
        }

        Customer[] destination = new Customer[count];
        System.arraycopy(result,0,destination,0,count);
        setCustomers(destination);
    }

    public void getCustomersSortedByNames() {
        Customer[] tempCustomers = customers;
        boolean isSortedByName = false;

        while (!isSortedByName) {
            isSortedByName = true;

            for (int i = 0; i < tempCustomers.length - 1; i++) {
                Customer tempCustomer = tempCustomers[i];
                Customer customerToCompare = tempCustomers[i + 1];

                boolean isTempTrainDestAndDepLarger = compareCustomersByNames(tempCustomer, customerToCompare);

                if (isTempTrainDestAndDepLarger) {
                    isSortedByName = false;

                    tempCustomers[i] = customerToCompare;
                    tempCustomers[i + 1] = tempCustomer;
                }
            }
        }

        setCustomers(tempCustomers);
    }

    private static boolean compareCustomersByNames(Customer tempCustomer, Customer customerToCompare) {
        String tempFirstName = tempCustomer.getFirstname();
        String firstNameToCompare = customerToCompare.getFirstname();

        String tempSurName = tempCustomer.getSurname();
        String surNameToCompare = customerToCompare.getSurname();

        String tempMiddleName = tempCustomer.getPatronymic();
        String middleNameToCompare = customerToCompare.getPatronymic();

        boolean isCurrentNameLarger = tempFirstName.compareTo( firstNameToCompare ) > 0;
        boolean areNamesEqual = tempFirstName.compareTo( firstNameToCompare ) == 0;
        boolean isCurrentSurNameLarger = tempSurName.compareTo( surNameToCompare ) > 0;
        boolean areSurNamesEqual = tempSurName.compareTo( surNameToCompare ) == 0;
        boolean isCurrentMidNameLarger = tempMiddleName.compareTo( middleNameToCompare ) > 0;

        boolean result = isCurrentNameLarger || (areNamesEqual && isCurrentSurNameLarger) || (areNamesEqual && areSurNamesEqual && isCurrentMidNameLarger);
        return result;
    }
}
